import React, { Component } from 'react';
import { connect } from 'react-redux'; 
import Stat from './Stat';

class App extends Component {

  //old no longer used when you use redux
  // state = {
  //   age:21
  // }

  // onAgeUp = () => {
  //   this.setState({
  //     ...this.state,
  //     age: ++this.state.age
  //   })
  // }

  // onAgeDown = () => {
  //   this.setState({
  //     ...this.state,
  //     age: --this.state.age
  //   })
  // }


  render() {
    return (
      <div>
        <div>Age: <span>{this.props.age}</span></div>
        <button onClick={this.props.onAgeUp}>Age UP</button>
        <button onClick={this.props.onAgeDown}>Age Down</button>
        <br/>
        <Stat value={this.props.age}/>
      </div>
    );
  }
}


const mapStateToProps =(state) => {
  return {
    age:state.age
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    onAgeUp: () => dispatch({type: 'AGE_UP'}),
    onAgeDown: () => dispatch({type: 'AGE_DOWN'})
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);


/**old code
 * 
 * 
 * import React, { Component } from 'react';
class App extends Component {

  state = {
    age:21
  }

  onAgeUp = () => {
    this.setState({
      ...this.state,
      age: ++this.state.age
    })
  }

  onAgeDown = () => {
    this.setState({
      ...this.state,
      age: --this.state.age
    })
  }


  render() {
    return (
      <div>
        <div>Age: <span>{this.state.age}</span></div>
        <button onClick={this.onAgeUp}>Age UP</button>
        <button onClick={this.onAgeDown}>Age Down</button>
      </div>
    );
  }
}

export default App;
 * 
 * 
 */