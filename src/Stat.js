import React from 'react';
import { Card } from 'react-bootstrap';

const StatCard = (props) => {

    return (
        <Card >
            <Card.Body >
                <h4>{`my age is: ${props.value}`}</h4>
            </Card.Body>
        </Card>
    )
};

export default StatCard;
